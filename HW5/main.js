let request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.type = 'json';

function createLoader(){
    let loader = document.createElement('div');
    loader.className = 'loader';
    document.body.prepend(loader);
    let button = document.querySelector('.button');
    button.remove();

}

request.onload = function () {
    setTimeout(() => {
        let loader = document.querySelector('.loader');
        loader.remove();

        let obj = request.response;
        obj = JSON.parse(obj);
        obj = obj.results;
        console.log(obj);

        obj.forEach(function (item) {
            function prtElementsArr(arr, item1, item2, item3) {
                let listBody = document.createElement('ul');
                document.body.prepend(listBody);
                listBody.innerHTML += `<li>EPISODE ID: ${item1}</li><li>EPISODE: ${item2}</li><li>DESCRIPTION: ${item3}</li>`;

                arr.map(function (i) {
                    listBody.innerHTML += `<li>${i}</li>`;
                })
            }

            prtElementsArr(item.characters, item.episode_id, item.title, item.opening_crawl);
        });
    }, 2000);

};






















