class Hamburger {
    constructor(options) {
        if (options.size === Hamburger.SIZE_SMALL || options.size === Hamburger.SIZE_LARGE) {
            this.size = options.size;
        } else {
            throw new HamburgerException("Вы неправильно указали размер!");
        }

        if (options.stuffing === Hamburger.STUFFING_CHEESE || options.stuffing === Hamburger.STUFFING_SALAD || options.stuffing === Hamburger.STUFFING_POTATO) {
            this.stuffing = options.stuffing;
        } else {
            throw new HamburgerException("Вы неправильно указали добавку!");
        }
    }


    addTopping(topping) {
        if (topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE) {
            this.topping = topping;
        } else {
            throw new HamburgerException("Вы неправильно указали поливку!");
        }
    }

    removeTopping(topping) {
        if (topping || this.topping === topping) {
            this.topping = undefined;
        } else {
            throw new HamburgerException("Эта поливка не установлена!");
        }
    }

    calculatePrice() {
        if (this.topping === undefined) {
            return this.size.price + this.stuffing.price;
        } else {
            return this.size.price + this.stuffing.price + this.topping.price;
        }
    }

    calculateCalories() {
        if (this.topping === undefined) {
            return this.size.calories + this.stuffing.calories;
        } else {
            return this.size.calories + this.stuffing.calories + this.topping.calories;
        }
    }

    getToppings() {
        return {
            toppings: {mayo: Hamburger.TOPPING_MAYO, spice: Hamburger.TOPPING_SPICE}
        }
    }

    getStuffings() {
        return {
            stuffing: {
                cheese: Hamburger.STUFFING_CHEESE,
                salad: Hamburger.STUFFING_SALAD,
                potato: Hamburger.STUFFING_POTATO,
            },
        }
    }

    getSize() {
        return {
            size: {small: Hamburger.SIZE_SMALL, large: Hamburger.SIZE_LARGE},
        }
    }
}

function HamburgerException(message) {
    this.message = message;
}

Hamburger.SIZE_SMALL = {price: 50, calories: 20};
Hamburger.SIZE_LARGE = {price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {price: 20, calories: 40};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {price: 15, calories: 0};

let hamburger = new Hamburger({size: Hamburger.SIZE_SMALL, stuffing: Hamburger.STUFFING_SALAD});

// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.removeTopping(Hamburger.TOPPING_MAYO);

console.log(hamburger.getToppings());

console.log(hamburger.calculatePrice())
console.log(hamburger.calculateCalories());

console.log(hamburger);