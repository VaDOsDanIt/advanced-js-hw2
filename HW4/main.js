class Column {

    createColumn() {
        event.preventDefault();
        let section = document.querySelector('.section');
        let column = document.createElement('div');
        column.className = 'container';
        column.innerHTML = ` <div class="container">
        <div class="header">
            <h2 class="title">To do</h2>
        </div>
        <div class="column"></div>
        <div class="footer">
            <a href="#" class="button">Add a card...</a>
        </div>
    </div>`;
        section.append(column);
        let cardContainer = column.querySelector('.column');
        let btnAddCard = column.querySelector('.button');

        btnAddCard.addEventListener('click', function (event) {
            Column.createCard(event, cardContainer);
        });

    }

    static createCard(event, cardContainer) {
        event.preventDefault();
        let card = document.createElement('div');
        card.className = 'card';
        card.setAttribute('draggable', 'true');
        card.innerHTML = `
                <input type="text" placeholder="Введите задачу...">
            `;
        cardContainer.append(card);
        Column.dragAndDrop();
    }

    static dragAndDrop() {
        console.log("Start DnD");
        const card = document.querySelectorAll('.card');
        const column = document.querySelectorAll('.column');


        let dragSrcEl = null;

        const dragStart = function (evt) {
            this.classList.add('draggable');
            setTimeout(() => {
                this.classList.add('hide');
                dragSrcEl = this;
            }, 0)
        };

        const dragEnd = function (evt) {
            this.classList.remove('hide');
            this.classList.remove('draggable');

        };

        const dragEnter = function (evt) {
            evt.preventDefault();
            this.classList.add('hovered');
            return true;
        };

        const dragLeave = function (evt) {
            this.classList.remove('hovered');
        };

        const dragOver = function (evt) {
            evt.preventDefault();
        };

        const dragDrop = function (evt) {
            if (dragSrcEl !== this && dragSrcEl !== null) {
                this.append(dragSrcEl);
                this.classList.remove('hovered');
            }

            evt.stopPropagation();
            return false;
        };


        column.forEach(function (columns) {
            columns.addEventListener('dragenter', dragEnter);
            columns.addEventListener('dragover', dragOver);
            columns.addEventListener('dragleave', dragLeave);
            columns.addEventListener('drop', dragDrop);
        });

        card.forEach(function (cards) {
            cards.addEventListener('dragstart', dragStart);
            cards.addEventListener('dragend', dragEnd);
        });
    };
}


let col = new Column();


