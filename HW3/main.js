class Game {
    constructor(options) {
        this.level = options.level;
    }

    start() {
        const td = document.querySelectorAll('td');
        let countRed = 0;
        let interval = 0;
        let countBlue = 0;
        let level = this.level.time;
        td[Math.floor(Math.random() * td.length)].style.backgroundColor = 'blue';
        td.forEach(function (item, i) {
            td[i].addEventListener('click', function () {
                if (this.style.backgroundColor === "blue") {
                    clearTimeout();
                    let d;
                    this.style.backgroundColor = 'white';
                    countBlue++;
                    d = Math.floor(Math.random() * td.length);
                    if(td[d].style.backgroundColor !== 'red') {
                        td[d].style.backgroundColor = 'blue';
                    }
                    interval = setInterval(function () {
                        if (td[d].style.backgroundColor === 'blue') {
                            countRed++;
                            if (td[d].style.backgroundColor !== 'red') {
                                td[d].style.backgroundColor = 'red';
                            }
                            if (countRed > 50) {
                                alert("Red Team Win!");
                                clearInterval(interval);
                            }
                            d = Math.floor(Math.random() * td.length);
                            td[d].style.backgroundColor = 'blue';
                        }
                    }, level);
                    if (countBlue > 50) {
                        alert("Blue Team Win!");
                        clearInterval(interval);
                    }
                }
            });
        });
    }
    newGame() {
        const td = document.querySelectorAll('td');
        td.forEach(function (i) {
           i.style.backgroundColor = "white";
        });

    }

}

Game.level_EASY = {time: 1500};
Game.level_MEDIUM = {time: 1000};
Game.level_HARD = {time: 500};

let easy = new Game({level: Game.level_EASY});
let medium = new Game({level: Game.level_MEDIUM});
let hard = new Game({level: Game.level_HARD});
// Появление квадрата после нажатия на него